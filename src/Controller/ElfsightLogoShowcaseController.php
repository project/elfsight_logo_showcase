<?php

namespace Drupal\elfsight_logo_showcase\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightLogoShowcaseController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/logo-showcase/?utm_source=portals&utm_medium=drupal&utm_campaign=logo-showcase&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
